# antyka_cantik ( Tugas Akhir WEB IoT)
## How to use 
1. start your own web server and database server
2. go to your web server directory with terminal/cmd, example : ``` cd C:\xampp\htdocs\ ```
3. Grab the project ``` git clone https://gitlab.com/daniltelkom/antyka_cantik.git ```
4. go to the project directory ``` cd antyka_cantik\laravel ```
5. ``` composer install ```
6. create your new database for this project
7. create your own .env file and configure it
8. migrate the database ``` php artisan migrate ``` 
9. configure config.js file (outside the laravel directory) with your own IP, port, and topic
10. don't forget to disable your firewall or configure your firewall (more secure)
# Have fun honey ^_^

